import os
import sys
import xml.etree.ElementTree as etree
from lxml import etree as et
import xlrd
import pandas as pd
import numpy as np
from collections import OrderedDict
from openpyxl import load_workbook
import xlwt

pathNPOLIS = "PACIENT/DPFS/NPOLIS"
pathFAM = "PACIENT/PERS/FAM"
pathIM = "PACIENT/PERS/IM"
pathOT = "PACIENT/PERS/OT"
pathDATE = "SLUCH/SL_COM/DATE_1"
pathKODVR = "SL_COM/KODVR"
book = xlwt.Workbook('utf8')

#df = pd.DataFrame(columns=('fioDoctor', 'Oshibka', 'Data', 'FAM', 'IM', 'OT', 'NPOLIS'))
#df2 = pd.DataFrame()
#df = pd.DataFrame(columns=('fioDoctor', 'Oshibka', 'Data', 'FAM', 'IM', 'OT', 'NPOLIS'))
#df2 = pd.DataFrame(columns=('fioDoctor', 'Oshibka', 'Data', 'FAM', 'IM', 'OT', 'NPOLIS'))
#s2 = pd.Series()
df2 = pd.DataFrame()
listBlockError = [] #тут будет собрана вся информация для каждой ошибки (Код врача.Дата.Полис.ФИО пац.Ошибка)
blockError = {}
idAndDiscription = {} #словарь с ключем ID услуги и расшифровкой ошибки (из файла fa)
listKodiVrachie = []
listKodiVrachieNoDuplicat = []
#pathFileReestr = input("Укажите xml файл реестра ")
#pathFileErrorReestr = input("Укажите xml файл с ошибками ")
#rb = xlrd.open_workbook(os.path.abspath('Personal.xls'),formatting_info=True)
#personalXls = rb.sheet_by_index(0)



data = pd.read_csv('Персонал.csv',sep=';')
data2 = data[['Ф.И.О. сотрудника','Рег. Код' ]]

def swapKodNaFIO (kodVracha):
    try:
        indexStroki = data2.loc[data2['Рег. Код'] == kodVracha ].index[0]
    except IndexError:
        return kodVracha
    return data2["Ф.И.О. сотрудника"][indexStroki]


def spisokKodovVrachei(KodVr):
    listKodiVrachie.append(KodVr)


#if len(pathFileReestr) < 1 or len(pathFileErrorReestr) < 1:
#    print ("Не указан файл")
#    sys.exit()

reestr = et.parse(os.path.abspath("A021_014612_191012.xml"))
#reestr = et.parse(os.path.abspath(pathFileReestr))
errorReestr = etree.parse(os.path.abspath('fA021_014612_191012.xml'))
#errorReestr = etree.parse(os.path.abspath(pathFileErrorReestr))
rootErrorReestr = errorReestr.getroot()
rootReestr = reestr.getroot()

for childOfRootError in rootErrorReestr.iter("PR"): #записываем в словарь ошибки и id
    idAndDiscription[childOfRootError.find("ID_SL").text] = childOfRootError.find("DESCRIPTION").text

for childOfRootReestr in rootReestr.iter("ZAP"):
    for childZap in childOfRootReestr.iter("SLUCH"):
        for childSluch in childZap.iter("ID_SL"):
            for idErr,Err in idAndDiscription.items():
                if idErr == childSluch.text:
                    fioVrach = swapKodNaFIO(childZap.find(pathKODVR).text)
                    spisokKodovVrachei(childZap.find(pathKODVR).text)
                    listBlockError.append(childZap.find(pathKODVR).text+";"+fioVrach+";"+Err+";"+childOfRootReestr.find(pathDATE).text+";"+childOfRootReestr.find(pathFAM).text+";"+childOfRootReestr.find(pathIM).text+";"+childOfRootReestr.find(pathOT).text+";"+childOfRootReestr.find(pathNPOLIS).text)


for  stroka in listBlockError:
    df =pd.DataFrame([stroka.split(';')],columns=('kodVracha','fioDoctor', 'Oshibka', 'Data', 'FAM', 'IM', 'OT', 'NPOLIS'))
    df2 = df2.append(df,ignore_index = True)

#filtered_data = df2[(df2.fioDoctor == "Коврижина Татьяна Георгиевна") ]

for kod in list(set(listKodiVrachie)):
    listKodiVrachieNoDuplicat.append(kod)

for kodVracha in listKodiVrachieNoDuplicat:
    filtered_data = df2[(df2.kodVracha == kodVracha )]
    #sheet = book.add_sheet(kodVracha)
    name = swapKodNaFIO(kodVracha)
    filtered_data.to_excel(name+".xlsx")
    print(filtered_data)

#book.save('filename.xls')
#book = load_workbook('test.xlsx')
#writer = pandas.ExcelWriter('test.xlsx', engine='openpyxl')
#writer.book = book


#df2.to_csv('df.csv', encoding='windows 1251', index=False)
#print (listKodiVrachieNoDuplicat)
#print (filtered_data)
#print (df2[["fioDoctor",'FAM']])
